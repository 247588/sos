#!bin/bash
find /usr/lib64/ -type f -name "libpipe*" -print -delete
#rm -rfv /usr/lib64/gnome-settings-daemon-3.0
find /usr/lib64/ -type f -name "libgtk*" -print -delete
find /usr/lib64/ -type f -name "*bluetooth*" -print -delete
#find /usr/lib64/ -type f -name "*gnome*" -print -delete
#find /usr/lib64/ -type f -name "*polkit*" -print -delete
find /usr/lib64/ -type f -name "*samba*" -print -delete
find /usr/lib64/ -type f -name "*libcol*" -print -delete
find /usr/lib64/ -type f -name "*jpg*" -print -delete
find / -iname "*jpg*" -print -delete
rm -rfv /usr/share/themes
find / -name "*svg*" -print -delete
find / -name "*icon*" -print -delete
find / -name "*gtk*" -print -delete
find / -name "*LLVM*" -print -delete
find / -name "*libbluetoth*" -print -delete
find / -name "*girepository*" -print -delete
find / -name "*aspell*" -print -delete
find / -name "*libdb-*" -print -delete
find / -name "*libedataserver*" -print -delete
find / -name "*libfdisk*" -print -delete
find / -name "*libgdk*" -print -delete
#find / -name "*libgnome*" -print -delete
find / -name "*libldb*" -print -delete
find / -name "*evolution*" -print -delete
find / -name "*libpoppler*" -print -delete
find / -name "*libQtDesigner*" -print -delete
find / -name "*libsamba-util*"  -print -delete
find / -name "*libsolv*" -print -delete
find / -name "*python3.6*" -print -delete
find / -name "*qml*" -print -delete
find / -name "*samba*" -print -delete
#find / -name "*gnome*" -print -delete
find / -name "*nm-*" -print -delete
find / -name "*gdm*" -print -delete
find / -name "*ldconfig*" -print -delete
find / -name "*anaconda*" -print -delete
find / -name "*backg*" -print -delete
find / -name "*doc*" -print -delete
find / -name "*gnupg*" -print -delete
find / -name "*help*" -print -delete
find / -name "*ibus*" -print -delete
find / -name "*libthai*" -print -delete
find / -name "*locale*" -print -delete
find / -name "*plymouth*" -print -delete
#find / -name "*polkit*" -print -delete
find / -name "*chinese*" -print -delete
find / -name "*japanese*" -print -delete
find / -name "*nese*" -print -delete
find / -name "*games*" -print -delete
find / -name "*rpm*" -print -delete
find / -name "*messages*" -print -delete
find / -name "*mail*" -print -delete
find / -name "*opt*" -print -delete
find / -name "*spool*" -print -delete
find / -name "*player*" -print -delete
find / -name "*zp*" -print -delete
find / -name "*zone*" -print -delete
find / -name "*them*" -print -delete
find / -name "*crack*" -print -delete
find / -name "*iris*" -print -delete
find / -name "*sss*" -print -delete
find / -name "*dnf*" -print -delete
find / -name "*blue*" -print -delete
find / -name "*libgtk*" -print -delete
find / -name "*color*" -print -delete
find / -name "*libquvi*" -print -delete
find / -name "*ldb*" -print -delete
find / -name "*gdb*" -print -delete
find / -name "*gtk*" -print -delete
find / -name "*libmozj*" -print -delete